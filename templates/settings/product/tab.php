<?php

/**
 * Template for settings tab on product page.
 */

?>
<li class="measurement_tab hide_if_grouped hide_if_variable">
	<a href="#measurement_product_data">
		<span><?php echo esc_html_x( 'Flexible Quantity', 'product_menu', 'flexible-quantity-core' ); ?></span>
	</a>
</li>
