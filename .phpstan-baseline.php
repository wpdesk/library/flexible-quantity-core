<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\Ajax\\\\CalculatorFormAjax\\:\\:calculator_form\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/Ajax/CalculatorFormAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\ProductPage\\:\\:render\\(\\) invoked with 1 parameter, 0 required\\.$#',
	'identifier' => 'arguments.count',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/Ajax/CalculatorFormAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Expression on left side of \\?\\? is not nullable\\.$#',
	'identifier' => 'nullCoalesce.expr',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/Ajax/CalculatorPriceAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\Ajax\\\\CalculatorPriceAjax\\:\\:price_calculation\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/Ajax/CalculatorPriceAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$price of method WC_Product\\:\\:set_price\\(\\) expects string, float given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/Ajax/CalculatorPriceAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\ProductPage\\:\\:get_attributes\\(\\) has parameter \\$input_attributes with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\ProductPage\\:\\:get_attributes\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\ProductPage\\:\\:get_measurement_value\\(\\) has parameter \\$input_attributes with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Page\\\\ProductPageScripts\\:\\:enqueue_scripts\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Page/ProductPageScripts.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_price\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_price\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_price_html\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_price_html\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_price_suffix\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_price_suffix\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_regular_price\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_regular_price\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_sale_price\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_sale_price\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_on_sale\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_on_sale\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_type\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_type\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method set_props\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method set_props\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Left side of && is always true\\.$#',
	'identifier' => 'booleanAnd.leftAlwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\SettingsContainer\\:\\:get\\(\\) expects WC_Product, WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\|WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_pricing_rules_price_html\\(\\) expects WC_Product, WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\|WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_sync\\(\\) expects WC_Product_Variable, WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\|WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_unsync\\(\\) expects WC_Product_Variable, WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\|WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\$product of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceHtml\\:\\:price_per_unit_html\\(\\) has invalid type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\$product of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceHtml\\:\\:price_per_unit_html\\(\\) has invalid type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\WC_Product_Variable\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceHtml.php',
];
$ignoreErrors[] = [
	'message' => '#^Cannot access property \\$cart_contents on array\\.$#',
	'identifier' => 'property.nonObject',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Function remove_filter invoked with 4 parameters, 2\\-3 required\\.$#',
	'identifier' => 'arguments.count',
	'count' => 6,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:add_cart_item\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:add_cart_item\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:after_cart_item_quantity_update\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:after_cart_item_quantity_update\\(\\) has parameter \\$cart with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_cart_item_from_session\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_cart_item_from_session\\(\\) has parameter \\$session_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_cart_item_from_session\\(\\) has parameter \\$values with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_price\\(\\) has parameter \\$price with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_regular_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_regular_price\\(\\) has parameter \\$price with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_regular_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_sale_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_sale_price\\(\\) has parameter \\$price with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:get_sale_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:unhooks\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_after_calculate_totals\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_after_calculate_totals\\(\\) has parameter \\$cart with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_before_calculate_totals\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_before_calculate_totals\\(\\) has parameter \\$cart with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_cart_item_subtotal\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_cart_item_subtotal\\(\\) has parameter \\$cart_item with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_cart_item_subtotal\\(\\) has parameter \\$cart_item_key with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Prices\\\\PriceModifier\\:\\:woocommerce_cart_item_subtotal\\(\\) has parameter \\$subtotal with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$value of method WC_Data\\:\\:update_meta_data\\(\\) expects array\\|string, true given\\.$#',
	'identifier' => 'argument.type',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\=\\=\\= between float and 0 will always evaluate to false\\.$#',
	'identifier' => 'identical.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Prices/PriceModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WC_Product and WC_Product will always evaluate to true\\.$#',
	'identifier' => 'instanceof.alwaysTrue',
	'count' => 2,
	'path' => __DIR__ . '/src/Hookable/Product/ProductModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\Ajax\\\\DimensionsAjax\\:\\:get_dimensions\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/Ajax/DimensionsAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\Ajax\\\\DimensionsAjax\\:\\:get_dimensions_definition_by_calculator_type\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/Ajax/DimensionsAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$post_id of class WPDesk\\\\Persistence\\\\Adapter\\\\WordPress\\\\WordpressPostMetaContainer constructor expects int, string given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/Ajax/DimensionsAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\Ajax\\\\SelectionAjax\\:\\:get_selected_options\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/Ajax/SelectionAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Template\\\\Selection\\\\Options\\\\OptionsProvider\\:\\:get_options\\(\\) invoked with 1 parameter, 0 required\\.$#',
	'identifier' => 'arguments.count',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/Ajax/SelectionAjax.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\CustomUnitsPage\\:\\:add_submenu_page\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/CustomUnitsPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\CustomUnitsPage\\:\\:admin_enqueue_scripts\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/CustomUnitsPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\CustomUnitsPage\\:\\:is_save_form_data_request\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/CustomUnitsPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\CustomUnitsPage\\:\\:render\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/CustomUnitsPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\CustomUnitsPage\\:\\:save_form_data_if_needed\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/CustomUnitsPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\CustomUnitsPage\\:\\:validate_form_data\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/CustomUnitsPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\ProductPage\\:\\:add_panel\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\ProductPage\\:\\:add_tab\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\SupportPage\\:\\:add_submenu_page\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/SupportPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\SupportPage\\:\\:admin_enqueue_scripts\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/SupportPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\SupportPage\\:\\:render\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/SupportPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\SupportPage\\:\\:\\$renderer \\(WPDesk\\\\View\\\\Renderer\\\\SimplePhpRenderer\\) does not accept WPDesk\\\\View\\\\Renderer\\\\Renderer\\.$#',
	'identifier' => 'assign.propertyType',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/SupportPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:add_column\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:add_column\\(\\) has parameter \\$columns with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:get_product_names\\(\\) has parameter \\$product_ids with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:get_term_names\\(\\) has parameter \\$term_ids with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:get_value\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:get_value\\(\\) has parameter \\$column with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:get_value\\(\\) has parameter \\$post_id with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:render_categories\\(\\) has parameter \\$category_ids with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:render_products\\(\\) has parameter \\$product_ids with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplateListingPage\\:\\:render_tags\\(\\) has parameter \\$tag_ids with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplateListingPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Settings\\\\TemplatePageScripts\\:\\:admin_enqueue_scripts\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Settings/TemplatePageScripts.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Calculator\\\\WeightCalculator and WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Calculator\\\\WeightCalculator will always evaluate to true\\.$#',
	'identifier' => 'instanceof.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Weight/WeightModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Weight\\\\WeightModifier\\:\\:get_cart_item_from_session\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Weight/WeightModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Weight\\\\WeightModifier\\:\\:get_cart_item_from_session\\(\\) has parameter \\$session_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Weight/WeightModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Weight\\\\WeightModifier\\:\\:get_cart_item_from_session\\(\\) has parameter \\$values with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Weight/WeightModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Hookable\\\\Weight\\\\WeightModifier\\:\\:\\$weight_calculator is never written, only read\\.$#',
	'identifier' => 'property.onlyRead',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Weight/WeightModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Unreachable statement \\- code above always terminates\\.$#',
	'identifier' => 'deadCode.unreachable',
	'count' => 1,
	'path' => __DIR__ . '/src/Hookable/Weight/WeightModifier.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\PluginConfig\\:\\:get_hookable_elements\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/PluginConfig.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$measurements$#',
	'identifier' => 'parameter.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Calculator/PriceCalculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Calculator\\\\WeightCalculator\\:\\:calculate_default_measurement_type\\(\\) invoked with 2 parameters, 1 required\\.$#',
	'identifier' => 'arguments.count',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Calculator/WeightCalculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_array\\(\\) with array will always evaluate to true\\.$#',
	'identifier' => 'function.alreadyNarrowedType',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:__construct\\(\\) has parameter \\$settings_bag with generic class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Collections\\\\SettingsBag but does not specify its types\\: TKey, T$#',
	'identifier' => 'missingType.generics',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_product\\(\\) has invalid return type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\WC_Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_product\\(\\) should return WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\WC_Product but returns WC_Product\\.$#',
	'identifier' => 'return.type',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_regular_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_sale_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_settings\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_sold_individually\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:set_pricing_rules\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:set_pricing_rules\\(\\) has parameter \\$pricing_rules with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$num of function abs expects float\\|int, string given\\.$#',
	'identifier' => 'argument.type',
	'count' => 2,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:\\$settings_bag with generic class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Collections\\\\SettingsBag does not specify its types\\: TKey, T$#',
	'identifier' => 'missingType.generics',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Return type \\(WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\WC_Product\\) of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Settings\\:\\:get_product\\(\\) should be covariant with return type \\(WC_Product\\) of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_product\\(\\)$#',
	'identifier' => 'method.childReturnType',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\SettingsBagFactory\\:\\:create\\(\\) return type with generic class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Collections\\\\SettingsBag does not specify its types\\: TKey, T$#',
	'identifier' => 'missingType.generics',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/SettingsBagFactory.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\SettingsContainer\\:\\:\\$collection with generic class Doctrine\\\\Common\\\\Collections\\\\ArrayCollection does not specify its types\\: TKey, T$#',
	'identifier' => 'missingType.generics',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/SettingsContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Template\\\\Selection\\\\Options\\\\TermOptionsProvider\\:\\:get_options_by_taxonomy\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Template/Selection/Options/TermOptionsProvider.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Services\\\\Template\\\\Selection\\\\Options\\\\TermOptionsProvider\\:\\:get_selected_terms_by_taxonomy\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Template/Selection/Options/TermOptionsProvider.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\TemplatePersistentContainer\\:\\:add\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/TemplatePersistentContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\TemplatePersistentContainer\\:\\:add\\(\\) has parameter \\$value with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/TemplatePersistentContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\TemplatePersistentContainer\\:\\:set\\(\\) has parameter \\$value with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/TemplatePersistentContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\!\\=\\= between array\\|float\\|int\\|string and null will always evaluate to true\\.$#',
	'identifier' => 'notIdentical.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/TemplatePersistentContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_numeric\\(\\) with float\\|int will always evaluate to true\\.$#',
	'identifier' => 'function.alreadyNarrowedType',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Validator/PositiveNumberValidator.php',
];
$ignoreErrors[] = [
	'message' => '#^Cannot assign new offset to string\\.$#',
	'identifier' => 'offsetAssign.dimType',
	'count' => 1,
	'path' => __DIR__ . '/src/Services/Validator/ValidationResult.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_pricing_label\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 3,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_pricing_rule_price_html\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_pricing_rules\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_pricing_unit\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method has_pricing_rules\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method pricing_rules_enabled\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Instantiated class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings not found\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\PricingTable\\:\\:get\\(\\) has parameter \\$atts with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\PricingTable\\:\\:get_pricing_rules_table\\(\\) has parameter \\$rules with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\PricingTable\\:\\:output\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\PricingTable\\:\\:output\\(\\) has parameter \\$atts with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\$settings of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\PricingTable\\:\\:get_pricing_rules_table\\(\\) has invalid type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\Shortcodes\\\\WC_Price_Calculator_Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/Shortcodes/PricingTable.php',
];
$ignoreErrors[] = [
	'message' => '#^@param tag must not be named \\$this\\. Choose a descriptive alias, for example \\$instance\\.$#',
	'identifier' => 'phpDoc.parseError',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Action callback returns float but should not return anything\\.$#',
	'identifier' => 'return.void',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Binary operation "\\*" between string and float\\|int results in an error\\.$#',
	'identifier' => 'binaryOp.invalid',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_numeric\\(\\) with float will always evaluate to true\\.$#',
	'identifier' => 'function.alreadyNarrowedType',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Class WC_Order_Item_Product referenced with incorrect case\\: WC_Order_item_Product\\.$#',
	'identifier' => 'class.nameCase',
	'count' => 5,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Function remove_filter invoked with 4 parameters, 2\\-3 required\\.$#',
	'identifier' => 'arguments.count',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WC_Order_Item_Product and WC_Order_Item_Product will always evaluate to true\\.$#',
	'identifier' => 'instanceof.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:add_to_cart_validation\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:add_to_cart_validation\\(\\) has parameter \\$variations with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:calculate_product_weights\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:display_product_data_in_cart\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:display_product_data_in_cart\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:display_product_data_in_cart\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_id\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_id\\(\\) has parameter \\$variation with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_item_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_item_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_item_from_session\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_item_from_session\\(\\) has parameter \\$values with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_item_from_session\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_cart_widget_item_price_html\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_item_price\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_item_quantity\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_measurement_cart_item_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_measurement_cart_item_data\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_measurement_cart_item_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:get_measurements_needed\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:humanize_cart_item_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:humanize_cart_item_data\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:humanize_cart_item_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:order_again_cart_item_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:order_again_cart_item_data\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:order_again_cart_item_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:round_measurement_needed_by_increment_precision\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:round_measurement_needed_by_increment_precision\\(\\) has parameter \\$measurement_needed with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:round_measurement_needed_by_increment_precision\\(\\) has parameter \\$settings with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:save_actual_unit_quantity\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:set_order_item_meta\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:set_order_item_meta\\(\\) has parameter \\$values with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:set_product_shipping_methods\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:set_product_shipping_methods\\(\\) has parameter \\$cart_item with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:setup_measurement_overage_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:setup_measurement_overage_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(Cart \\$this Cart instance\\)\\: Unexpected token "\\$this", expected variable at offset 267 on line 7$#',
	'identifier' => 'phpDoc.parseError',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$variation of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:calculate_measurement_needed\\(\\) expects WC_Product_Variation\\|null, WC_Product\\|false\\|null given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#4 \\$round of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:calculate_price\\(\\) expects bool, WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Cart\\:\\:\\$measurements_needed type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Right side of && is always true\\.$#',
	'identifier' => 'booleanAnd.rightAlwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\=\\=\\= between false and float will always evaluate to false\\.$#',
	'identifier' => 'identical.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$message might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 4,
	'path' => __DIR__ . '/src/WooCommerce/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$ID on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$price_ex_tax on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$price_inc_tax on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$regular_price_ex_tax on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$regular_price_inc_tax on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$sale_price_ex_tax on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to property \\$sale_price_inc_tax on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined method WC_Order_Item\\:\\:get_product\\(\\)\\.$#',
	'identifier' => 'method.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined method WC_Product\\:\\:get_variation_price\\(\\)\\.$#',
	'identifier' => 'method.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined method WC_Product\\:\\:get_variation_regular_price\\(\\)\\.$#',
	'identifier' => 'method.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined method WC_Product\\:\\:get_variation_sale_price\\(\\)\\.$#',
	'identifier' => 'method.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function in_array\\(\\) requires parameter \\#3 to be set\\.$#',
	'identifier' => 'function.strict',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Left side of && is always true\\.$#',
	'identifier' => 'booleanAnd.leftAlwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Compatibility\\:\\:catalog_visibility_options_pricing_calculator_quantity_input\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Compatibility\\:\\:google_product_feed_pricing_rules_price_adjustment\\(\\) has invalid return type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_sync\\(\\) expects WC_Product_Variable, WC_Product given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_unsync\\(\\) expects WC_Product_Variable, WC_Product given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\$feed_item of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Compatibility\\:\\:google_product_feed_pricing_rules_price_adjustment\\(\\) has invalid type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\StdClass\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Ternary operator condition is always true\\.$#',
	'identifier' => 'ternary.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Compatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_callable\\(\\) with \'is_ajax\' will always evaluate to true\\.$#',
	'identifier' => 'function.alreadyNarrowedType',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method convert_alpha_country_code\\(\\) on an unknown class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Country_Helper\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Country_Helper not found\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WC_Product and WC_Product will always evaluate to true\\.$#',
	'identifier' => 'instanceof.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:array_insert_after\\(\\) has parameter \\$array with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:array_insert_after\\(\\) has parameter \\$element with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:array_insert_after\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:array_to_xml\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:array_to_xml\\(\\) has parameter \\$element_key with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:array_to_xml\\(\\) has parameter \\$element_value with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:maybe_doing_it_early\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:trigger_error\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:wc_add_notice\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Helper\\:\\:wc_print_notice\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\=\\=\\= between false and string will always evaluate to false\\.$#',
	'identifier' => 'identical.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable property access on mixed\\.$#',
	'identifier' => 'property.dynamicName',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^@param string \\$unformatted_stock does not accept actual type of parameter\\: float\\|int\\.$#',
	'identifier' => 'parameter.phpDocType',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WC_Product and WC_Product will always evaluate to true\\.$#',
	'identifier' => 'instanceof.alwaysTrue',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between string and WC_Product will always evaluate to false\\.$#',
	'identifier' => 'instanceof.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:after_cart_item_quantity_update\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:cart_shipping_packages\\(\\) has parameter \\$packages with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:cart_shipping_packages\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:get_availability_measurement\\(\\) has parameter \\$return with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:get_availability_measurement\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:get_checkout_item_quantity\\(\\) has parameter \\$values with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:get_order_item_measurement_quantity\\(\\) has parameter \\$item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:get_widget_cart_item_quantity\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:order_again_item_set_quantity\\(\\) has parameter \\$items with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:order_again_item_set_quantity\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Inventory\\:\\:woocommerce_quantity_input_step_admin\\(\\) has parameter \\$step with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Offset \'_measurement_needed\' on string in isset\\(\\) does not exist\\.$#',
	'identifier' => 'isset.offset',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(string\\|int\\)\\: Unexpected token "\\\\n\\\\t \\* ", expected variable at offset 251 on line 6$#',
	'identifier' => 'phpDoc.parseError',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$value of class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement constructor expects float\\|int, string\\|null given\\.$#',
	'identifier' => 'argument.type',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Result of && is always false\\.$#',
	'identifier' => 'booleanAnd.alwaysFalse',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Inventory.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_string\\(\\) with float\\|int will always evaluate to false\\.$#',
	'identifier' => 'function.impossibleType',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:__construct\\(\\) has parameter \\$options with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:get_conversion_table\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:get_normalize_table\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:get_options\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:get_standard_unit\\(\\) should return string but returns null\\.$#',
	'identifier' => 'return.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:set_value\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:\\$conversion_table type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:\\$is_editable has no type specified\\.$#',
	'identifier' => 'missingType.property',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:\\$normalize_table type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\:\\:\\$options type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\!\\=\\= between float and 0 will always evaluate to true\\.$#',
	'identifier' => 'notIdentical.alwaysTrue',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Measurement.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Persistence\\\\WooCommerceProductContainer\\:\\:set\\(\\) has parameter \\$value with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Persistence/WooCommerceProductContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\!\\=\\= between array\\|float\\|int\\|string and null will always evaluate to true\\.$#',
	'identifier' => 'notIdentical.alwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Persistence/WooCommerceProductContainer.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$max_variation_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$max_variation_regular_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$max_variation_sale_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$min_variation_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$min_variation_regular_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$min_variation_sale_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_max_variation_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_max_variation_regular_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_max_variation_sale_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_min_variation_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_min_variation_regular_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_min_variation_sale_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product_Variable\\:\\:\\$wcmpc_price\\.$#',
	'identifier' => 'property.notFound',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WC_Product and WC_Product will always evaluate to true\\.$#',
	'identifier' => 'instanceof.alwaysTrue',
	'count' => 5,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:calculate_price\\(\\) has parameter \\$settings with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:calculator_enabled\\(\\) has parameter \\$settings with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_other_measurement\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_other_measurement\\(\\) has parameter \\$product with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_pricing_rules_price_html\\(\\) has parameter \\$settings with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_product_measurement\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_product_measurement\\(\\) has parameter \\$product with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_product_measurement\\(\\) has parameter \\$settings with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_product_measurements\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:get_quantity_range\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:pricing_calculator_enabled\\(\\) has parameter \\$settings with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:shipping_calculator_inventory_enabled\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:shipping_calculator_inventory_enabled\\(\\) has parameter \\$product with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_sync\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_unsync\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$value of class WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement constructor expects float\\|int, string given\\.$#',
	'identifier' => 'argument.type',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable method call on WC_Product\\.$#',
	'identifier' => 'method.dynamicName',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Product.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$text of function esc_attr expects string, int given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductLoop.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\PluginConfig\\:\\:get_full_core_path\\(\\)\\.$#',
	'identifier' => 'staticMethod.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Left side of && is always true\\.$#',
	'identifier' => 'booleanAnd.leftAlwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:__construct\\(\\) has parameter \\$plugin_url with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:add_price_html_filters\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:add_weight_per_unit_label_filter\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:available_variation\\(\\) has parameter \\$variation_data with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:available_variation\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:enqueue_frontend_scripts\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:remove_max_quantity_calculated_inventory\\(\\) has parameter \\$max_value with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:remove_weight_per_unit_label_filter\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:render_embedded_styles\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:render_price_calculator\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Negated boolean expression is always false\\.$#',
	'identifier' => 'booleanNot.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(string\\|int\\|float\\|null The max value\\)\\: Unexpected token "The", expected variable at offset 536 on line 9$#',
	'identifier' => 'phpDoc.parseError',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$price of function wc_price expects float, string given\\.$#',
	'identifier' => 'argument.type',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_sync\\(\\) expects WC_Product_Variable, WC_Product given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$product of static method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Product\\:\\:variable_product_unsync\\(\\) expects WC_Product_Variable, WC_Product given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#3 \\$deps of function wp_register_script expects array\\<string\\>, null given\\.$#',
	'identifier' => 'argument.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#4 \\$ver of function wp_enqueue_script expects bool\\|string\\|null, float given\\.$#',
	'identifier' => 'argument.type',
	'count' => 3,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\ProductPage\\:\\:\\$plugin_url has no type specified\\.$#',
	'identifier' => 'missingType.property',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Right side of && is always true\\.$#',
	'identifier' => 'booleanAnd.rightAlwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Unreachable statement \\- code above always terminates\\.$#',
	'identifier' => 'deadCode.unreachable',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$settings might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/ProductPage.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function array_filter\\(\\) requires parameter \\#2 to be passed to avoid loose comparison semantics\\.$#',
	'identifier' => 'arrayFilter.strict',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_array\\(\\) with array will always evaluate to true\\.$#',
	'identifier' => 'function.alreadyNarrowedType',
	'count' => 3,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_basic_regular_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_calculator_measurements\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_default_settings\\(\\) is unused\\.$#',
	'identifier' => 'method.unused',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_default_settings\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_input_attributes\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_measurement_types\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_options\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_pricing_rule_price_html\\(\\) has parameter \\$rule with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_pricing_rules\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_pricing_rules_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_regular_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_sale_price\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_settings\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_settings_pricing_rules\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_settings_shipping_rules\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_shipping_class_id\\(\\) should return float but returns false\\.$#',
	'identifier' => 'return.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:get_sold_individually\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:is_decimals_enabled\\(\\) should return array\\<WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Measurement\\> but returns bool\\.$#',
	'identifier' => 'return.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:is_pricing_table_enabled\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:is_shipping_table_enabled\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:set_pricing_rules\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:set_pricing_rules\\(\\) has parameter \\$pricing_rules with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:set_raw_settings\\(\\) has parameter \\$settings with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:set_raw_settings\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:set_raw_settings\\(\\) should return array but returns null\\.$#',
	'identifier' => 'return.type',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:update_settings\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:update_settings\\(\\) is unused\\.$#',
	'identifier' => 'method.unused',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$num of function abs expects float\\|int, string given\\.$#',
	'identifier' => 'argument.type',
	'count' => 2,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:\\$pricing_rules type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Settings\\:\\:\\$settings type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Result of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Persistence\\\\WooCommerceProductContainer\\:\\:set\\(\\) \\(void\\) is used\\.$#',
	'identifier' => 'method.void',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Shortcodes\\:\\:pricing_table_shortcode\\(\\) has parameter \\$atts with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Shortcodes.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:area_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:convert_units_to_simple_array\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:convert_units_to_simple_array\\(\\) has parameter \\$units with no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:custom_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:dimension_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_all\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_all_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_calculator_type\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_calculator_type\\(\\) has parameter \\$unit with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_custom_units\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_unit_options\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:get_unit_options\\(\\) has parameter \\$unit with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:other_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:unit_select\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:unit_select\\(\\) has parameter \\$field_args with no type specified\\.$#',
	'identifier' => 'missingType.parameter',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:volume_dimensions_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:volume_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\Units\\:\\:weight_units\\(\\) return type has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/Units.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\WooCompatibility\\:\\:wc_deprecated_function\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/WooCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\WooCompatibility\\:\\:wc_doing_it_wrong\\(\\) has no return type specified\\.$#',
	'identifier' => 'missingType.return',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/WooCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Offset \'body\' on array\\{headers\\: WpOrg\\\\Requests\\\\Utility\\\\CaseInsensitiveDictionary, body\\: string, response\\: array\\{code\\: int, message\\: string\\}, cookies\\: array\\<int, WP_Http_Cookie\\>, filename\\: string\\|null, http_response\\: WP_HTTP_Requests_Response\\} in isset\\(\\) always exists and is not nullable\\.$#',
	'identifier' => 'isset.offset',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/WooCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\$date of method WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\WooCompatibility\\:\\:wc_format_datetime\\(\\) has invalid type WPDesk\\\\Library\\\\FlexibleQuantityCore\\\\WooCommerce\\\\SV_WC_DateTime\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/WooCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Right side of && is always true\\.$#',
	'identifier' => 'booleanAnd.rightAlwaysTrue',
	'count' => 1,
	'path' => __DIR__ . '/src/WooCommerce/WooCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method getString\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 9,
	'path' => __DIR__ . '/templates/settings/basic-settings/basic-settings.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$available_units has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/basic-settings/basic-settings.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/basic-settings/basic-settings.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method __\\(\\) on an unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/templates/settings/custom-units-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method output_render\\(\\) on an unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/custom-units-page.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$renderer contains unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/custom-units-page.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$translate contains unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/custom-units-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$units might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/custom-units-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$is_locked might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/custom-units/custom-units-row.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method bag\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 8,
	'path' => __DIR__ . '/templates/settings/dimensions/dimension-row.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method getString\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/dimensions/dimension-row.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/dimensions/dimension-row.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$units has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/dimensions/dimension-row.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method __\\(\\) on an unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/templates/settings/dimensions/dimensions.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method getString\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/dimensions/dimensions.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/dimensions/dimensions.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$translate contains unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/dimensions/dimensions.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$names might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/listing-page-column.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$title might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/listing-page-column.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_boxes\\(\\) on an unknown class MarketingBoxes\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/marketing-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Negated boolean expression is always false\\.$#',
	'identifier' => 'booleanNot.alwaysFalse',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/marketing-page.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$boxes contains unknown class MarketingBoxes\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/marketing-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$is_locked might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/marketing-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$translate might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 3,
	'path' => __DIR__ . '/templates/settings/marketing-page.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method __\\(\\) on an unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method bag\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method getString\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method output_render\\(\\) on an unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$renderer contains unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$translate contains unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/pricing-table/pricing-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$template_url might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/product/panel.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$nonce_action might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/selection-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$nonce_name might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/selection-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$pre_select_product_id might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/selection-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$selection_category might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 3,
	'path' => __DIR__ . '/templates/settings/selection-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$translate might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/selection-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method __\\(\\) on an unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method bag\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method getString\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method output_render\\(\\) on an unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$renderer contains unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$translate contains unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/shipping-table/shipping-table.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$names might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-listing/assign-column.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$title might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-listing/assign-column.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method __\\(\\) on an unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method bag\\(\\) on an unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 4,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method output_render\\(\\) on an unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 4,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$available_units has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$renderer contains unknown class Renderer\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class SettingsBag\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$translate contains unknown class Translate\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/settings/template-section.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_type\\(\\) on an unknown class Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_unit\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_calculator_type_derived\\(\\) on an unknown class Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_sold_individually\\(\\) on an unknown class Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method render_single_measurement_field\\(\\) on an unknown class ProductPage\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$controller contains unknown class ProductPage\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$measurements contains unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$product contains unknown class Product\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$product_measurement contains unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$settings contains unknown class Settings\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/price-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$calculator_mode might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/quantity-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$measurements might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/quantity-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$product_measurement might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 7,
	'path' => __DIR__ . '/templates/single-product/quantity-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$total_price might not be defined\\.$#',
	'identifier' => 'variable.undefined',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/quantity-calculator.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_label\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_name\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_unit\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_unit_common\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method get_unit_label\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 2,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to method is_editable\\(\\) on an unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$attributes has no value type specified in iterable type array\\.$#',
	'identifier' => 'missingType.iterableValue',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var for variable \\$measurement contains unknown class Measurement\\.$#',
	'identifier' => 'class.notFound',
	'count' => 1,
	'path' => __DIR__ . '/templates/single-product/single-measurement-field.php',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
