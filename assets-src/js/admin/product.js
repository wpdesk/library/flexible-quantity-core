jQuery(($) => {
	const $disabledPriceInfo = $('.js-disabled-price-info');
	const $priceInput = $('#_regular_price');
	const $salePriceInput = $('#_sale_price');
	console.log('script loaded');

	if ($disabledPriceInfo.length) {
		console.log('should trigger');
		$priceInput.prop('readonly', true);
		$salePriceInput.prop('readonly', true);
	}
});
