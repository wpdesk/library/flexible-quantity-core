## [2.2.1] - 2025-03-03
### Fixed
* Disabled use product price option for free version

## [2.2.0] - 2025-02-26
### Added
* Added support for measurement values, pricing table and shipping table up to 4 decimal places
* Added product price option into template options

## [2.1.9] - 2025-01-09
### Fixed
* Different sold individually option description in free version

## [2.1.8] - 2025-01-02
### Fixed
* Enhance sold individually option description

## [2.1.7] - 2024-11-27
### Fixed
* Validation issue where adding a quantity to the cart failed despite being a multiple of the increment value. The calculation logic has been corrected.
* Added a user-friendly error message when attempting to add products to the cart with stock below 1

# [2.1.6] - 2024-11-25
### Fixed
* Disable stock quantity below 1, now any quantity below one is out of stock

## [2.1.5] - 2024-11-25
### Fixed
* Fixed shopping cart subtotal calculations, when sold individually option is on

## [2.1.4] - 2024-11-20
### Fixed
* Fixed shopping cart subtotal calculations
* Fixed DivisionByZeroError when increment is 0
* Allow incorrect stock status display when pricien quantity less than 1 unit

## [2.1.3] - 2024-10-30
### Fixed
* Fixed incorrect stock status display when product quantity was less than 1 unit
* Fixed pricing table calculations not working correctly in shopping cart

## [2.1.2] - 2024-10-30
### Fixed
* Fixed incorrect stock status display when product quantity was less than 1 unit
* Fixed pricing table calculations not working correctly in shopping cart

## [2.1.1] - 2024-08-09
### Fixed
* No product page calculator settings, when price is defined only in pricing table

## [2.1.0] - 2024-08-07
### Added
* Custom hook for measurement defalt value

## [2.0.4] - 2024-07-25
### Fixed
* Unit translations (checkout, cart)

## [2.0.3] - 2024-07-18
### Fixed
* Product page unit translations

## [2.0.2] - 2024-07-15
### Fixed
* Product page price calculations when more than one dimension is used
* Product page add to cart validation

## [2.0.1] - 2024-07-10
### Fixed
* Cart total weight calculations

## [2.0.0] - 2024-07-04
### Added
* Major Update!
* Templates for calculator settings (accessible through a separate menu)
* Support for product variations

## [1.1.12] - 2024-04-09
### Fixed
- Rounding precision when increment is not defined

## [1.1.11] - 2024-03-12
### Fixed
- Unit conversion not precise enough

## [1.1.10] - 2024-03-05
### Fixed
- Some valid values are not passing the increment validation function
- Unit conversion not precise for some units (factor 12)
- Price per unit html do not show price sufix

## [1.1.9] - 2023-12-13
### Fixed
- Fixed an issue with calculating sq. in. to sq. ft., causing validation errors when adding to the cart.
- Resolved an issue where orders could not be saved in the admin area when the quantity was fractional.
## [1.1.8] - 2023-10-03
### Fixed
- Fixed an issue where the Pricing Table was not aware of WooCommerce's quantity, and it only considered the plugin's quantity.
- Resolved an problem related to adding items to the cart, which depended on the chosen WooCommerce quantity. This issue affected whether a new item was created or the quantity was added to an existing item in the cart.
## [1.1.7] - 2023-09-25
### Fixed
- Validation (when sold individually is on) ignors what comes after the decimal point.
## [1.1.6] - 2023-03-07
### Fixed
- Stock availability translation
## [1.1.5] - 2022-12-13
### Fixed
- Values calculation on product page
## [1.1.4] - 2022-10-27
### Fixed
- translation
## [1.1.3] - 2022-10-07
### Fixed
- hide custom units for locked version

## [1.1.2] - 2022-10-05
### Fixed
- division by zero error
## [1.1.1] - 2022-10-04
### Fixed
- custom units error

## [1.1.0] - 2022-10-03
### Added
- custom units
- decimals in quantity

## [1.0.10] - 2022-07-13
### Fixed
- units calculation
## [1.0.9] - 2022-06-30
### Added
- units translations

## [1.0.8] - 2022-06-13
### Fixed
- removed old translations
## [1.0.7] - 2022-06-06
### Fixed
- minor fixes
## [1.0.6] - 2022-06-02
### Fixed
- clean security issues
## [1.0.5] - 2022-06-02
### Fixed
- minor fixes
## [1.0.4] - 2022-06-01
### Fixed
- sanitization
## [1.0.3] - 2022-05-27
### Fixed
- get support url
## [1.0.2] - 2022-05-27
### Added
- licence file
### Fixed
- marketing page
## [1.0.1] - 2022-05-26
### Fixed
- woocommerce tooltips
- marketing page
## [1.0] - 2022-05-25
### Added
- init
