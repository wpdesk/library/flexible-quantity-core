<?php

namespace WPDesk\Library\FlexibleQuantityCore\Services\Template\Selection\Options;

use WPDesk\Library\FlexibleQuantityCore\Hookable\PostType\FQTemplateType;

abstract class OptionsProviderBase {

	/**
	 * @var string
	 */
	protected $search = '';

	/**
	 * @var int
	 */
	protected $page = 1;

	protected const ITEMS_PER_PAGE = 10;

	abstract protected function get_template_meta_key(): string;

	/**
	 * @param string $search
	 */
	public function set_search( string $search ): void {
		$this->search = $search;
	}

	/**
	 * @param int $page
	 */
	public function set_page( int $page ): void {
		$this->page = $page;
	}

	protected function get_offset(): int {
		return ( $this->page - 1 ) * self::ITEMS_PER_PAGE;
	}

	/**
	 * Returns IDs that should be excluded in options.
	 *
	 * @return int[]
	 */
	protected function get_excluded_ids(): array {
		global $wpdb;

		$results = $wpdb->get_col(
			$wpdb->prepare(
				"
				SELECT pm.meta_value
				FROM {$wpdb->postmeta} pm
				INNER JOIN {$wpdb->posts} pt ON pm.post_id = pt.ID
				WHERE pm.meta_key = %s
				AND pt.post_type = %s
				",
				$this->get_template_meta_key(),
				FQTemplateType::POST_TYPE
			)
		);

		return \array_map( 'intval', $results );
	}

	/**
	 * @param array<int, array<string, string>> $options
	 * @param int $total_records
	 *
	 * @return array <string, array <mixed, mixed>>
	 */
	protected function prepare_response( array $options, int $total_records ): array {
		return [
			'options'    => $options,
			'pagination' => [
				'more' => $this->get_offset() + count( $options ) < $total_records,
			],
		];
	}
}
