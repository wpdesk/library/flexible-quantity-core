<?php

namespace WPDesk\Library\FlexibleQuantityCore\Services\Validator;

use WPDesk\Library\FlexibleQuantityCore\WooCommerce\Measurement;

interface ValidatorInterface {

	/**
	 * Validate measurement input data.
	 *
	 * @param Measurement $measurement
	 *
	 * @return string
	 */
	public function validate( Measurement $measurement ): string;
}
