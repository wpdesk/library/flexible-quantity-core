<?php
namespace WPDesk\Library\FlexibleQuantityCore\Hookable\PostType;

use WPDesk\PluginBuilder\Plugin\Hookable;

class FQTemplateType implements Hookable {
	const POST_TYPE = 'fq_template';

	public function hooks(): void {
		add_action( 'init', $this, 10 );
	}

	/**
	 * Register post types.
	 */
	public function __invoke(): void {
		\register_post_type(
			self::POST_TYPE,
			[
				'labels'              => [
					'name'                => __( 'Template', 'flexible-quantity-core' ),
					'singular_name'       => __( 'Template', 'flexible-quantity-core' ),
					'menu_name'           => __( 'Flexible Quantity', 'flexible-quantity-core' ),
					'parent_item_colon'   => '',
					'all_items'           => __( 'Templates', 'flexible-quantity-core' ),
					'view_item'           => __( 'View Templates', 'flexible-quantity-core' ),
					'add_new_item'        => __( 'Add New Template', 'flexible-quantity-core' ),
					'add_new'             => __( 'Add New', 'flexible-quantity-core' ),
					'edit_item'           => __( 'Edit Template', 'flexible-quantity-core' ),
					'update_item'         => __( 'Save Template', 'flexible-quantity-core' ),
					'search_items'        => __( 'Search Template', 'flexible-quantity-core' ),
					'not_found'           => __( 'Template not found', 'flexible-quantity-core' ),
					'not_found_in_trash'  => __( 'Template not found in trash', 'flexible-quantity-core' ),
				],
				'description'         => __( 'Flexible Quantity Templates', 'flexible-quantity-core' ),
				'public'              => false,
				'show_ui'             => true,
				'capability_type'     => 'post',
				'capabilities'        => [],
				'map_meta_cap'        => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'hierarchical'        => false,
				'query_var'           => true,
				'supports'            => [ 'title' ],
				'has_archive'         => false,
				'show_in_nav_menus'   => true,
				'show_in_menu'        => true,
				'menu_icon'           => 'dashicons-calculator',
			]
		);
	}
}
