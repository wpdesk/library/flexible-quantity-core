<?php

use WPDesk\Codeception\Tests\Acceptance\Cest\AbstractCest;

class ActivationCest extends AbstractCest {

	/**
	 * Deactivate plugins before tests.
	 *
	 * @param AcceptanceTester $i .
	 *
	 * @throws \Codeception\Exception\ModuleException .
	 */
	public function _before( AcceptanceTester $i ) {
		$i->loginAsAdmin();
		$i->amOnPluginsPage();
	}

	/**
	 * Plugin activation.
	 *
	 * @param AcceptanceTester $i .
	 *
	 * @throws \Codeception\Exception\ModuleException .
	 */
	public function pluginActivation( AcceptanceTester $i ) {
		$i->deactivatePlugin( $this->getPluginSlug() );
		$i->deactivatePlugin( 'woocommerce' );

		$i->seePluginDeactivated( $this->getPluginSlug() );

		$i->activateWPDeskPlugin(
			$this->getPluginSlug(),
			[ 'woocommerce' ],
			[ 'The “Flexible Quantity” plugin cannot run without WooCommerce active. Please install and activate WooCommerce plugin.' ]
		);

	}
}
