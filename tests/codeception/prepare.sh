if [ ! -d $DEPENDENT_PLUGINS_DIR/flexible-invoices-woocommerce ]
then
    echo "Flexible Invoices WooCommerce"
    /tmp/clone.sh git@gitlab.com:wpdesk/heroes/flexible-invoices-woocommerce.git $DEPENDENT_PLUGINS_DIR/flexible-invoices-woocommerce master
    cd $DEPENDENT_PLUGINS_DIR/flexible-invoices-woocommerce/
    composer install
    composer install --no-dev
    cd /tmp
fi
