<?php

use PHPUnit\Framework\TestCase;
use WPDesk\Library\FlexibleQuantityCore\WooCommerce\Measurement;


class MeasurementTest extends TestCase {

	/**
	 * The allowed precision when doing unit conversion
	 */
	private const CONVERSION_ACCEPTED_PRECISION = 0.00001;

	public function getData() {
		$conversion_table = Measurement::get_conversion_table();

		$data = [];
		foreach ( $conversion_table as $common_unit => $child_units ) {
			foreach ( $child_units as $child_unit => $factor ) {
				$data[] = [ $common_unit, $child_unit ];
			}
		}

		return $data;
	}

	/**
	 * Conversion to one unit and back gives should give as the same value as the original value
	 *
	 * @dataProvider getData
	 */
	public function testUnitConversionPrecision( $common_unit, $child_unit ) {
		$start_value = 1;
		$value_common_unit = Measurement::convert( $start_value, $child_unit, $common_unit);
		$value_child_unit = Measurement::convert( $value_common_unit, $common_unit, $child_unit );
		$this->assertEqualsWithDelta( $start_value, $value_child_unit, self::CONVERSION_ACCEPTED_PRECISION, "Failed to convert $child_unit to $common_unit" );
	}
}
